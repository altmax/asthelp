package astHelp

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/format"
	"go/printer"
	"go/token"
	"strings"
)

func CreateBlockStmt(list []ast.Stmt) *ast.BlockStmt {
	return &ast.BlockStmt{
		List: list,
	}
}

func CreateFuncStmt(fn string) ast.Stmt {
	return &ast.ExprStmt{
		X: &ast.CallExpr{
			Fun: &ast.Ident{Name: fn},
			Args: []ast.Expr{
				&ast.BasicLit{Kind: token.INT, Value: "42"},
			},
		},
	}
}

func CreateBinaryStmt(left, right string) ast.Stmt {
	return &ast.ExprStmt{
		X: &ast.BinaryExpr{
			X:  &ast.Ident{Name: left},
			Op: token.DEFINE,
			Y:  &ast.BasicLit{Value: right},
		},
	}
}

func CreateField(name, typ string, tags ...string) *ast.Field {
	var value string
	if len(tags) > 0 {
		for i, v := range tags {
			value += v
			if i != len(tags)-1 {
				value += " "
			}
		}
		value = "`" + value + "`"
	}
	switch name {
	case "id", "url", "ttl", "Id", "Url", "Ttl":
		name = strings.ToUpper(name)
	case "type", "Type":
		if name == "type" {
			name = "typ"
		} else if name == "Type" {
			name = "Typ"
		}
	}
	f := &ast.Field{
		Names: []*ast.Ident{&ast.Ident{
			Name: name,
		}},
		Type: &ast.Ident{Name: typ},
		Tag: &ast.BasicLit{
			Value: value,
		},
	}
	return f
}

func CreateStruct(name string, fields []*ast.Field) *ast.GenDecl {
	// list := []*ast.Field{}
	// list = append(list, fields...)

	s := &ast.GenDecl{
		Tok: token.TYPE,
		Specs: []ast.Spec{&ast.TypeSpec{
			Name: &ast.Ident{Name: name},
			Type: &ast.StructType{
				Fields: &ast.FieldList{
					List: fields,
				},
			},
		}},
	}
	return s
}

func CreateFile(packageName string, decls []ast.Decl) *ast.File {
	return &ast.File{
		Name:  &ast.Ident{Name: packageName},
		Decls: decls,
	}
}

func PrintAST(f *ast.File) []byte {
	printConfig := &printer.Config{Mode: printer.TabIndent, Tabwidth: 4}
	var buf bytes.Buffer
	err := printConfig.Fprint(&buf, token.NewFileSet(), f)
	if err != nil {
		panic(err)
	}
	out := buf.Bytes()
	fmt.Println(string(out))
	out, err = format.Source(out)
	if err != nil {
		panic(err)
	}
	return out
}

func CreateFunction(doc []*ast.Comment, recv []*ast.Field, name string, params []*ast.Field, results []*ast.Field, body *ast.BlockStmt) *ast.FuncDecl {
	return &ast.FuncDecl{
		Doc:  &ast.CommentGroup{List: doc},
		Recv: &ast.FieldList{List: recv},
		Name: &ast.Ident{Name: name},
		Type: CreateFuncType(params, results),
		Body: body,
	}
}

func CreateFuncType(params []*ast.Field, results []*ast.Field) *ast.FuncType {
	return &ast.FuncType{
		Func: token.NoPos,
		Params: &ast.FieldList{
			List: params,
		},
		Results: &ast.FieldList{
			List: results,
		},
	}
}

//TransName transform name to LowerCase and CamelCase to under_score
func TransName(str string) string {
	var newstr string
	for i, v := range str {
		s := string(v)
		if v >= 65 && v <= 90 {
			s = string(v + 32)
			if i != 0 {
				s = "_" + s
			}
		}
		newstr += s
	}
	return newstr
}
